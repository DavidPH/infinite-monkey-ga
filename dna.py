import random
import string


def random_char():
    return random.choice(string.ascii_letters + string.punctuation + " ")


class Dna:
    def __init__(self, len):
        self.len = len
        self._fitness = 0
        self.genes = []

        # Generate random string of length num
        for i in range(len):
            self.genes.append(random_char())

    def fitness(self, target):
        for i, char in enumerate(self.genes):
            if char == target[i]:
                self._fitness += 1
        return self._fitness

    def crossover(self, other):
        child = Dna(self.len)
        mid_point = random.randrange(0, self.len)
        for i in range(self.len):
            if i > mid_point:
                child.genes[i] = self.genes[i]
            else:
                child.genes[i] = other.genes[i]
        return child

    def mutate(self, rate):
        for i in range(self.len):
            if random.random() < rate:
                self.genes[i] = random_char()
        return self

    def __str__(self):
        return "".join(self.genes)
