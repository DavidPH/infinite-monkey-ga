from dna import *


# Manages the array of all the populations
class Population:
    def __init__(self, target, mutation_rate, size):
        self.generation = 0
        self.target = target
        self.mutation_rate = mutation_rate
        self.size = size
        self.population = [Dna(len(target)) for _ in range(size)]
        self.fitness_lst = [self.population[i].fitness(self.target) for i in range(size)]
        self.pool = []

    def pick_one(self):
        sum_fitness = sum(self.fitness_lst)
        r = random.randrange(0, sum_fitness + 1)
        i = 0
        while r > 0:
            r = r - self.fitness_lst[i]
            i += 1
        i -= 1
        return self.population[i]

    def pick_best(self):
        return max(self.population, key=lambda x: x.fitness(self.target))

    # Pick two objects from pool, higher fitness more likely
    # crossover
    # mutate
    # add to new pop
    def new_generation(self):
        new_pop = []
        for i in range(self.size):
            a = self.pick_one()
            b = self.pick_one()
            new_pop.append(a.crossover(b).mutate(self.mutation_rate))
        self.population = new_pop
        self.fitness_lst = [self.population[i].fitness(self.target) for i in range(self.size)]
        self.generation += 1

    def __str__(self):
        return "\n".join([str(x) for x in self.population])