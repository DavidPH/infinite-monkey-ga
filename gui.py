import tkinter as tk
import tkinter.scrolledtext as scrolledtext


class EntryWithPlaceholder(tk.Entry):
    def __init__(self, master=None, placeholder="PLACEHOLDER", color='grey'):
        super().__init__(master)

        self.placeholder = placeholder
        self.placeholder_color = color
        self.default_fg_color = self['fg']

        self.bind("<FocusIn>", self.foc_in)
        self.bind("<FocusOut>", self.foc_out)

        self.put_placeholder()

    def put_placeholder(self):
        self.insert(0, self.placeholder)
        self['fg'] = self.placeholder_color

    def foc_in(self, *args):
        if self['fg'] == self.placeholder_color:
            self.delete('0', 'end')
            self['fg'] = self.default_fg_color

    def foc_out(self, *args):
        if not self.get():
            self.put_placeholder()


class Application:
    def __init__(self, top, func):
        font1 = "-family {Segoe UI} -size 13"
        font2 = "-family {Segoe UI} -size 10"
        top.geometry("587x590")
        top.minsize(587, 590)
        top.title("Genetic Algorithm")
        top.configure(background="#d9d9d9")

        self.func = func
        self.running = False

        self.Entry = EntryWithPlaceholder(top, placeholder="Enter target string to evolve into")
        self.Entry.place(relx=0.443, rely=0.016, height=34, relwidth=0.552)
        self.Entry.configure(font=font1)

        self.run_btn = tk.Button(top, command=self.btn_click)
        self.run_btn.place(relx=0.034, rely=0.018, height=34, width=97)
        self.run_btn.configure(background="#d9d9d9")
        self.run_btn.configure(text="RUN")

        self.body_textbox = scrolledtext.ScrolledText(top)
        # self.body_textbox = tk.Text(top)
        self.body_textbox.place(relx=0.034, rely=0.152, relheight=0.694
                                , relwidth=0.919)
        self.body_textbox.configure(font=font1)

        self.bottom_textbox = tk.Text(top)
        self.bottom_textbox.place(relx=0.034, rely=0.913, relheight=0.050
                                  , relwidth=0.919)
        self.bottom_textbox.configure(font=font1)

        self.curr_gen_strvar = tk.StringVar()
        self.curr_gen_strvar.set("Generation 0")
        self.curr_generation_label = tk.Label(top, textvariable=self.curr_gen_strvar)
        self.curr_generation_label.place(relx=0.034, rely=0.09, height=29, width=126)
        self.curr_generation_label.configure(background="#d9d9d9")
        self.curr_generation_label.configure(font=font2)
        self.curr_generation_label.configure(relief="groove")

        self.best_gen_strvar = tk.StringVar()
        self.best_gen_strvar.set("Best of Gen 0")
        self.best_gen_label = tk.Label(top, textvariable=self.best_gen_strvar)
        self.best_gen_label.place(relx=0.034, rely=0.859, height=29, width=126)
        self.best_gen_label.configure(background="#d9d9d9")
        self.best_gen_label.configure(font=font2)
        self.best_gen_label.configure(relief="groove")

        self.pop_size = EntryWithPlaceholder(top, "500")
        self.pop_size.place(relx=0.221, rely=0.016, height=34, relwidth=0.092)
        self.pop_size.configure(font=font1)

        self.mutation_rate = EntryWithPlaceholder(top, "0.01")
        self.mutation_rate.place(relx=0.33, rely=0.016, height=34, relwidth=0.092)
        self.mutation_rate.configure(font=font1)


    def btn_click(self):
        if not self.running:
            self.running = True
            self.run_btn.configure(text="STOP")
            for best, pop in self.func(self.Entry.get(), int(self.pop_size.get()), float(self.mutation_rate.get())):
                self.curr_gen_strvar.set(f"Generation {pop.generation}")
                self.best_gen_strvar.set(f"Best of Gen {pop.generation}")
                self.body_textbox.delete(1.0, "end")
                self.bottom_textbox.delete(1.0, "end")
                self.body_textbox.insert(1.0, pop)
                self.bottom_textbox.insert(1.0, best)
                self.body_textbox.update()
                self.body_textbox.see("end")
                self.bottom_textbox.update()
                if not self.running:
                    break
        self.running = False
        self.run_btn.configure(text="RUN")
