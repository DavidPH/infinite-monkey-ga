import gui
from population import *
import tkinter as tk


def genetic_algorithm(target, pop_size=500, mutation_rate=0.01):
    population = Population(target, mutation_rate, pop_size)
    while True:
        best = str(population.pick_best())
        yield best, population
        if best == target:
            break
        population.new_generation()


if __name__ == "__main__":
    root = tk.Tk()
    app = gui.Application(root, genetic_algorithm)
    root.mainloop()
